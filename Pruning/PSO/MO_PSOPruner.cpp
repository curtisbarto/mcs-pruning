/*
 * MO_PSO.cpp
 *
 *  Created on: Nov 12, 2010
 *      Author: rgreen
 */

#include "MO_PSOPruner.h"

#include <map>
#include <vector>
#include "PSOPruner.h"
#include "Utils.h"

#include <iostream>

using namespace std;

MO_PSOPruner::MO_PSOPruner(int popSize, int generations, Classifier* o, std::vector<Generator> g, std::vector<Line> l, double p, bool ul)
         : MOPruner(popSize, generations, g, l, o, p, ul){
    Init(popSize, generations, o, g, l, p, ul);
    
    cFactor = 1;
}

void MO_PSOPruner::Init(int popSize, int generations, Classifier* o, std::vector<Generator> g, std::vector<Line> l, double p, bool ul){
    MOPruner::Init(popSize, generations, g, l, o, p, ul);
    
    if(useLines && lines.size() > 0){
        Nd = g.size() + l.size();
        //C1 = -4.9445;   C2      =  1.91636;
        //C3 =  -1.07072; C4      = 3.74248;
        //W  = 0.22292;   cFactor = 0.705276 ;
         C1 = -3.30092, C2 = -4.59481,
        C3 = 3.42747,  C4 = -1.58556,
        C5 = 3.24961,  C6 = -4.76997,
        W = 2.93164;
    }else{
        Nd = g.size();
        C1 = -3.30092, C2 = -4.59481,
        C3 = 3.42747,  C4 = -1.58556,
        C5 = 3.24961,  C6 = -4.76997,
        W = 2.93164;
    }
}

void MO_PSOPruner::setParams(double c1, double c2, double c3, double c4, double w){
    C1 = c1; C2 = c2;
    C3 = c3; C4 = c4;
    W  = w;
}

void MO_PSOPruner::Reset(int np, int nt){
    MOPruner::Reset(np, nt);
    clearTimes();
    clearVectors();
}


void MO_PSOPruner::clearVectors(){
    MOPruner::clearVectors();
    swarm.clear();

}
MO_PSOPruner::~MO_PSOPruner() {
    // TODO Auto-generated destructor stub
}
void MO_PSOPruner::initPopulation(MTRand& mt){

    swarm.clear(); gBest.clear(); gBestValues.clear();
    gBest.resize(3, std::vector<int>(gens.size(), 0));

    #ifdef _MSC_VER
        gBestValues.push_back(-std::numeric_limits<double>::infinity()); // Max
        gBestValues.push_back(-std::numeric_limits<double>::infinity()); // Max
    #else
        gBestValues.push_back(-INFINITY); // Max
        gBestValues.push_back(-INFINITY); // Max
    #endif

    for(int i=0; i<Np; i++){
        MO_BinaryParticle p;

        std::vector<int> t1(Nd, 0), t2(Nd, 0);
        p.pBest.push_back(t1);
        p.pBest.push_back(t2);

        #ifdef _MSC_VER
            p.pBestValues.push_back(-std::numeric_limits<double>::infinity()); // Max
            p.pBestValues.push_back(-std::numeric_limits<double>::infinity()); // Max
        #else
            p.pBestValues.push_back(-INFINITY); // Max
            p.pBestValues.push_back(-INFINITY); // Max
        #endif

        for(int j=0; j<(int)gens.size(); j++){
            p.pos.push_back((mt.rand() < gens[j].getOutageRate() ? 0 : 1 ));
            p.vel.push_back(gens[j].getOutageRate());
        }
        
        if(useLines && lines.size() > 0){
            int linesDown = 0;
            for(int j=0; j<(int)lines.size(); j++){
                p.pos.push_back(1);
                if(p.pos[j] == 0) linesDown++;
                p.vel.push_back(lines[j].getOutageRate());
            }
        }
        swarm.push_back(p);
    }
}
void MO_PSOPruner::evaluateFitness(MTRand& mt){
    std::vector<double> results;
    for(unsigned int i=0; i<swarm.size(); i++){

        results = EvaluateSolution(swarm[i].pos);
        swarm[i].fitness.clear();
        swarm[i].fitness = results;

        //Local Best
        if(results[0] > swarm[i].pBestValues[0]){
            swarm[i].pBest[0]       = swarm[i].pos;
            swarm[i].pBestValues[0] = results[0];
        }
        if(results[1] > swarm[i].pBestValues[1]){
            swarm[i].pBest[1]       = swarm[i].pos;
            swarm[i].pBestValues[1] = results[1];
        }		
        //Global Best
        if(results[0] > gBestValues[0]){
            gBest[0] 		= swarm[i].pos;
            gBestValues[0] 	= results[0];
        }
        if(results[1] > gBestValues[1]){
            gBest[1] 		= swarm[i].pos;
            gBestValues[1] 	= results[1];
        }
        
    }
}
void MO_PSOPruner::updatePositions(MTRand& mt){
    double newV;
    for(unsigned int i=0; i<swarm.size(); i++){
        for(unsigned int j=0; j<gens.size(); j++){
            newV = cFactor * (W * swarm[i].vel[j] +
                    C1 * mt.rand() * (swarm[i].pBest[0][j] 	- swarm[i].pos[j]) +
                    C2 * mt.rand() * (gBest[0][j] 			- swarm[i].pos[j]) +
                    C3 * mt.rand() * (swarm[i].pBest[1][j] 	- swarm[i].pos[j]) +
                    C4 * mt.rand() * (gBest[1][j] 			- swarm[i].pos[j]));
                    
            newV = sigMoid(newV);
            swarm[i].vel[j] = newV;
            if(newV > mt.rand()) { swarm[i].pos[j] = 1;}
            else                 { swarm[i].pos[j] = 0;}
        }
    }
}

void MO_PSOPruner::Prune(MTRand& mt){

    timer.startTimer();

    initPopulation(mt);
    totalIterations = 0;
    while(!isConverged()){
        evaluateFitness(mt);
        updatePositions(mt);
        totalIterations++;
    }
    timer.stopTimer();
    pruningTime = timer.getElapsedTime();
}

double MO_PSOPruner::sigMoid(double v){
    return 1/(1+exp(-v));
}

