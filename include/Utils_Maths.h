#ifndef UTILS_MATHS_H
#define UTILS_MATHS_H

#include <algorithm>
#include <fstream>
#include <iomanip>
#include <iterator>
#include <math.h>
#include <sstream>
#include <stdio.h>
#include <string>
#include <vector>

//#include <gsl/gsl_rng.h>
//#include <gsl/gsl_randist.h>

#ifndef _OPENMP
    #include "omp.h"
#endif

#include “Utils.h”
#include "anyoption.h"
#include "Bus.h"
#include "defs.h"
#include "Generator.h"
#include "Line.h"
#include "MTRand.h"
#include "Primes.h"
#include "RandomNumbers.h"

using namespace Maths {

    extern double sigMoid(double v);
    extern int factorial(int n);
    extern int combination(int n, int r);
    extern void twoByTwoCholeskyDecomp(std::vector<std::vector<double> >& A);
    extern double unitStep(double X);
    extern std::vector< std::vector<int> > matMult(std::vector< std::vector<int> > A, std::vector< std::vector<int> > B);
    extern double expm (double p, double ak);
    extern double series(int m, int id);
    extern double piNumber(int sampleNumber);
    extern std::vector<double> decToBin(double n, int numDigits);


};
#endif